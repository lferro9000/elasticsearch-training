# Random notes

- remove apple quarantine flags from binaries / folders

xttr -d -r com.apple.quarantine kibana

On certain versions, depending on how macos is setup, it can trip gatekeeper, forfeiting the start of the execution.

- monitoring in 8.x

xpack.monitoring.collection.enabled is set to false by default

- encryption issues

[2022-04-15T13:00:32.519+01:00][warn ][plugins.alerting] APIs are disabled because the Encrypted Saved Objects plugin is missing encryption key. Please set xpack.encryptedSavedObjects.encryptionKey in the kibana.yml or use the bin/kibana-encryption-keys command.

- Add information to

  Node Roles
  Shapshoting

- Review content so far

- Settings to explore

  xpack.monitoring.collection.enabled is set to false

  auto-expand replicas

- Searching data when outside of the scope of the data type of the field mapping

  how to ensure that all the search items are returned as expected when the data is expected to be
