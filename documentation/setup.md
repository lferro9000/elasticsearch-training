# Setup a cluster

## Binary setup

### Download

Download:

- https://www.elastic.co/downloads/elasticsearch
- https://www.elastic.co/downloads/kibana

For your platform.

Extract the files and move the to the folder of your choice.

For easier work, we created a folder inside this project called stack, with subfolders elasticsearch and kibana, without platform or versions.

### Setup

Start elasticsearch with:

    cd stack/elasticsearch
    bin/elasticsearch

After a while, the following will show (with variants, depending on versions of elasticsearch etc):

```
Elasticsearch security features have been automatically configured!
✅ Authentication is enabled and cluster connections are encrypted.

ℹ️  Password for the elastic user (reset with `bin/elasticsearch-reset-password -u elastic`):
  just-a-password-here

ℹ️  HTTP CA certificate SHA-256 fingerprint:
  just-a-certificate-fingerprint-here

ℹ️  Configure Kibana to use this cluster:
• Run Kibana and click the configuration link in the terminal when Kibana starts.
• Copy the following enrollment token and paste it into Kibana in your browser (valid for the next 30 minutes):
  just-a-base64-encoded-token==

ℹ️  Configure other nodes to join this cluster:
• On this node:
  ⁃ Create an enrollment token with `bin/elasticsearch-create-enrollment-token -s node`.
  ⁃ Uncomment the transport.host setting at the end of config/elasticsearch.yml.
  ⁃ Restart Elasticsearch.
• On other nodes:
  ⁃ Start Elasticsearch with `bin/elasticsearch --enrollment-token <token>`, using the enrollment token that you generated.

```

After that, repeat the same for kibana:

    cd ..
    cd kibana
    bin/kibana

You will get a url to hit with your browser similar to:

    http://localhost:5601/?code=115819

Open it and you will get prompted to insert the previous indicated:

    just-a-base64-encoded-token==

Insert it and complete the configuration.

After that, you can login with user elastic and the **just-a-password-here** as password (change to the values shown on your terminal!).

### Issues with restarting

> NOTE: needs cleanup and review

Reference: https://discuss.elastic.co/t/elasticsearch-stopping-and-getting-network-exception/224515

Stack trace:

```
org.elasticsearch.bootstrap.StartupException: java.lang.IllegalArgumentException: No up-and-running site-local (private) addresses found, got [name:lo0 (lo0), name:en0 (en0), name:awdl0 (awdl0), name:llw0 (llw0), name:utun0 (utun0), name:utun1 (utun1), name:utun2 (utun2)]
```

The relevant part of the configuration is:

https://www.elastic.co/guide/en/elasticsearch/reference/current/modules-network.html

The issue is that the network names and how they work have changed recently, both on linux and macos, there may be issues with setting up automated named networks.

The workaround is to setup a fixed ip address:

    network.host: 10.0.0.100

To do that on macos, go to network interfaces, and create a "fake" interface, based on an existing active interface. Then on advanced tab, setup a manual / staci IP address that doesn't clash with any existing network interfaces.

The recommended interfaces are the usual "universal" local addresses like 127.x.x.x or 10.x.x.x.

But as wifi usually use 10.x.x.x, ensure that the range you are using is different from your current wifi connection one.

Then go to elasticsearch.yml located on ./stack/elasticsearch/config and uncomment the network.host and set it for the ip address you selected.

Then elasticsearch will start, and using the credentials elastic and the set password, you will be able to get the health check:

    https:/10.0.0.100:9200

On the kibana setup, you will need to change the automatic setup to map for that ip address as well.

From:

    elasticsearch.hosts: ['https://192.168.1.150:9200']

To:

    elasticsearch.hosts: ['https://10.0.0.100:9200']

This should be done on config.yml, at the end of the file as the entries there are setup accordingly to the automated configuration done with the initial setup.

Then one needs to replace the certificate with one that uses the static ip address otherwise, one gets:

```[2022-04-15T13:18:07.155+01:00][ERROR][elasticsearch-service] Unable to retrieve version information from Elasticsearch nodes. Hostname/IP does not match certificate's altnames: IP: 10.0.0.100 is not in the cert's list: 2a00:23c4:ff91:4400:443c:2964:4a83:8ecb, fe80::e446:33ff:fe66:96fb, 2a00:23c4:ff91:4400:14f5:97f1:201b:165, fdaa:bbcc:ddee:0:1c05:f62a:ea0e:65ba, fe80::57b7:c7df:4dd9:f381, fe80::1, 2a00:23c4:ff91:4400:c68:48ac:f904:fb33, fe80::cd:7872:28c:fb91, 192.168.1.150, 192.168.1.167, fe80::8460:9259:c6a:c7a, fdaa:bbcc:ddee:0:1033:e689:f804:14c9, fe80::ce81:b1c:bd2c:69e, fe80::1055:c559:d959:d53f, ::1, 127.0.0.1, 2a00:23c4:ff91:4400:8d96:1fdb:f9be:e04c

```

when kibana tries to connect to the remapped ip address.

As that is complex, the best approach is to use the same ip address as it was configured initially: 192.168.1.150

So on the network setup in macos:

- setup a fixed static address with the same address as shown in the automated configuration in ./stack/kibana/config/kibana.yml
- change ./stack/elasticsearch/config/elasticsearch.yml to that address in:

  network.host: 192.168.1.150

- restart elasticsearch (and wait until it finishes)
- restart kibana

### Setting up a fixed IP address on a network interface (macos)

https://www.macinstruct.com/tutorials/how-to-set-a-static-ip-address-on-a-mac/

### Issues with network picking a public IP address

elasticsearch doesn't start if it picks a public ip address with the default configuration

https://bugs.launchpad.net/charm-elasticsearch/+bug/1714126

Private (site-local) ranges are:

    192.168.0.0/16, 172.16.0.0/12 and 10.0.0.0/8

Setting network.host may fix it:

    network.host: 0.0.0.0

But as other effects that may not be desired (as making the ES server available to anyone that can reach the server regardless of the network interface used).

### HCL Terraform issues with the elasticsearch component

https://support.hcltechsw.com/csm?id=kb_article&sysparm_article=KB0094637

Similar issue with relation to network configuration.

## Setup with docker

In this setup, we follow the proposed setup from elastic.co site, with some adaptations.

The adaptations are minor to ensure that data is stored in a local folder.

This setup also allows a more easy upgrade / downgrate path for trying out different versions of elasticsearch, so it can be used to target a specific deploy.

The setup is stored in ./docker-compose.yml at the root of the project and the data is stored in ./stack/data folder.

To start it, just type docker-compose up --build on first time.

It takes quite a bit to start at the first time as it generates a "setup" image to trigger the CA / certificate generation and then reuse them to create a 3 node cluster of elasticsearch with one kibana container linked to it.

This setup, as it uses docker, is not prone to network misconfigurations.

Reference: https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html

Another reference: https://github.com/pires/docker-elasticsearch#environment-variables
