# Bulk requests for data ingestion

The basic way to handle bulk api data ingestion is:

    curl -XPOST "localhost:9200/_bulk?pretty" -H "Content-Type: application/x-ndjson" --data-binary @example.json

If security / authentication is active, amend to:

    curl -XPOST "localhost:9200/_bulk?pretty" -u -H "Content-Type: application/x-ndjson" --data-binary @example.json

## Using the sample data from the course

File: https://github.com/codingexplained/complete-guide-to-elasticsearch/blob/master/products-bulk.json

If you want to follow the exercises, you will need that data file.

The whole repo is marked Apache 2.0 Licensed as well (likewise for this repo).

    curl -XPOST "https://192.168.1.150:9200/_bulk?pretty" -u -u elastic:elasticpassword -H "Content-Type: application/x-ndjson" --data-binary @products-bulk.json

Just replace the elasticpassword with the one setup when creating the stack: [just-a-password-here](./authentication.md)

In the file [bulk_ingestion_of_data.http](../requests/bulk_ingestion_of_data.http) you will find the equivalent request done with the REST vscode plugin.

In either case, you should receive a response of:

```
HTTP/1.1 200 OK
X-elastic-product: Elasticsearch
content-type: application/json
content-length: 168740

{
  "took": 2325,
  "errors": false,
  "items": [
    {
      "index": {
        "_index": "products",
        "_id": "1",
        "_version": 1,
        "result": "created",
        "_shards": {
          "total": 3,
          "successful": 3,
          "failed": 0
        },
        "_seq_no": 16,
        "_primary_term": 3,
        "status": 201
      }
    },
    [...]
    {
      "index": {
        "_index": "products",
        "_id": "1000",
        "_version": 1,
        "result": "created",
        "_shards": {
          "total": 3,
          "successful": 3,
          "failed": 0
        },
        "_seq_no": 470,
        "_primary_term": 3,
        "status": 201
      }
    }
  ]
}
```

The [...] means that there are an huge amound of similar data in between.

## Create your own dataset in python

We will cover how to create your own datasets (with the use of the python library [faker](https://faker.readthedocs.io/en/master/) and other tools).

It has three simple phases:

- Modeling the data
- Run a generator with the modeling (and export into a bulk api compatible ndjson file)
- Import it on Elasticsearch

As extra we will learn also how to generate index mappers from the modeling of the data. Those are used for search optimization on the data.
