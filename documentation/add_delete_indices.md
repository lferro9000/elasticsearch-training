# Add and delete indices

When creating a new index, one can specify the number of shards and the number of replicas. The number of replicas excludes the main dataset. So if one states that wants 3 replicas, that will be 4 copies of the data. The main plus the 3 replicas.

The number of shards indicates in how many chunks the data should be spread.

There are no hard values for the correct values for this as that depends on the resources available per node in the cluster, the data that we are storing on it, the type and quantity of queries, the number of nodes and the resource use of all things that contribute to the use of the cluster.

Please check on ./requests/add_delete_index.http for examples.

This is also where alias, routing and mappings are defined, among other settings.

Of notice, some settings are not editable after the index is created. To change them, it is required to reindex (aka create a new index from the old one).

## Acknowledged meaning

Reference: https://discuss.elastic.co/t/does-elastic-searchs-acknowledged-true-means-actually-all-of-indices-are-have-been-created/216608/3

and

https://www.elastic.co/guide/en/elasticsearch/reference/current/indices-create-index.html#indices-create-api-query-params

When elasticsearch responds to a request with acknowledged=true, it means that the master node and all related nodes accepted the request successfully.

It doesn't mean the request finished. For that use either the /\_cat/tasks or the health endpoints to check if it finished and the cluster is in the correct state before continuing.

In the end, the result of the task is expected to complete successfully with all the changes available after the next refresh tick:

https://discuss.elastic.co/t/does-elastic-searchs-acknowledged-true-means-actually-all-of-indices-are-have-been-created/216608/8
