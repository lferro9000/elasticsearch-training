# Inspecting the cluster

After setup the authentication, one can start poke around in it to explore what is already there.

Open the file ./requests/inspect_cluster.md and run it with VSCode

Then execute the requests as they are shown.
