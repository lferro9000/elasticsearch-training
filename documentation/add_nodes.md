# Add a new nodes ES 8.x

Reference: https://www.elastic.co/guide/en/elasticsearch/reference/current/configuring-stack-security.html#stack-enroll-nodes

On the initial elasticsearch folder, run:

    bin/elasticsearch-create-enrollment-token -s node

To generate a node token.

This will generate a temporary token like:

```
eyJ2ZXIiOiI4LjEuMiIsImFkciI6WyIxOTIuMTY4LjEuMTUwOjkyMDAiXSwiZmdyIjoiNDIxOTI3ODQ5Mzc2OThjNGQwZDFjYzVhZjAzZThjZWRmNTcwNWYyNmNiNmVkZjc0YTQ4N2FlMTZmYjZkNGE0YyIsImtleSI6ImtjakVNb0FCc0RSUjYtYzN6RTBVOlQ2ZjNDa01uUTBpaUNBREUwUDBxS1EifQ==
```

Then on the folder with the elasticsearch for the new node:

    bin/elasticsearch --enrollment-token eyJ2ZXIiOiI4LjEuMiIsImFkciI6WyIxOTIuMTY4LjEuMTUwOjkyMDAiXSwiZmdyIjoiNDIxOTI3ODQ5Mzc2OThjNGQwZDFjYzVhZjAzZThjZWRmNTcwNWYyNmNiNmVkZjc0YTQ4N2FlMTZmYjZkNGE0YyIsImtleSI6ImtjakVNb0FCc0RSUjYtYzN6RTBVOlQ2ZjNDa01uUTBpaUNBREUwUDBxS1EifQ==

You can reuse the enrollment token for as many nodes as intended.

> This base / default configuration is good enough for a development local stack, not for production.
