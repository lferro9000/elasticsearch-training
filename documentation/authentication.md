# Authentication token

Authentication is required and mandatory on newer versions of elasticsearch by default.

Since we are not changing that, we will need to handle it.

For interacting with older versions of ES or versions where authentication is turned off, skip this steps / configuration.

## Generate the token from username / password

Setup environment variables for both your ES_USERNAME and ES_PASSWORD:

    EXPORT ES_USERNAME=elastic
    EXPORT ES_PASSWORD=just-a-password-here

See in [Setup a cluster](./setup.md) for the ES_PASSWORD details (by searching just-a-password-here).

Then open python in a terminal, and execute the following code:

    import base64
    import os
    username=os.environ.get("ES_USERNAME")
    password=os.environ.get("ES_PASSWORD")
    encoded = f"{username}:{password}".encode("UTF8")
    print(base64.b64encode(encoded))

Add the output as an environment variable as well for future use on all requests, as required:

    EXPORT ES_TOKEN=b'the-output-of-previous-code=='

Then you will be able to perform all commands as indicated on the requests folder using a plain http / rest client.

Please check .env_example for the environment variables needed to be setup and the kind of values you will find.

DO NOT TRY to use that password as it will not work :)
