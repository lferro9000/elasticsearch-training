# Elasticsearch Exercises

This is a list of all the exercises I'm doing in preparation for the Elasticsearch Engineer Certification.

The source reference materials are the certification itself, a couple of online training courses I took, and a all the experiance and use cases I face with the related topics.

Some references are short on details in some sections, when that happens, full exercises to expose the subject will be added.

The idea is to fully cover the basics of elasticsearch and the topics required to master in the EE Certification.

There will be constant reviews of all the materials for now, as I'm actively persuing this certification.

## Resources

The first resource is obviously the certification topics list present on elastic.co website.

As of the time of writing, the topics are:

(Elastic Certified Engineer Exam)[https://www.elastic.co/training/elastic-certified-engineer-exam]

The second resource is a basic elasticsearch training course from (Bo Anderson)[https://www.udemy.com/course/elasticsearch-complete-guide/#instructor-1], in Udemy:

(Elasticsearch Complete Guide)[https://www.udemy.com/course/elasticsearch-complete-guide/]

## Topics

\* Denote new topics from last review

### Data Management

- Define an index that satisfies a given set of requirements
- \* Use the Data Visualizer to upload a text file into Elasticsearch
- Define and use an index template for a given pattern that satisfies a given set of requirements
- Define and use a dynamic template that satisfies a given set of requirements
- \* Define an Index Lifecycle Management policy for a time-series index
- \* Define an index template that creates a new data stream

### Searching Data

- Write and execute a search query for terms and/or phrases in one or more fields of an index
- Write and execute a search query that is a Boolean combination of multiple queries and filters
- \* Write an asynchronous search
- Write and execute metric and bucket aggregations
- Write and execute aggregations that contain sub-aggregations
- Write and execute a query that searches across multiple clusters

### Developing Search Applications

- Highlight the search terms in the response of a query
- Sort the results of a query by a given set of requirements
- Implement pagination of the results of a search query
- Define and use index aliases
- Define and use a search template

### Data Processing

- Define a mapping that satisfies a given set of requirements
- Define and use a custom analyzer that satisfies a given set of requirements
- Define and use multi-fields with different data types and/or analyzers
- Use the Reindex API and Update By Query API to reindex and/or update documents
- Define and use an ingest pipeline that satisfies a given set of requirements, including the use of Painless to modify documents
- Configure an index so that it properly maintains the relationships of nested arrays of objects

### Cluster Management

- Diagnose shard issues and repair a cluster's health
- Backup and restore a cluster and/or specific indices
- \* Configure a snapshot to be searchable
- Configure a cluster for cross-cluster search
- \* Implement cross-cluster replication
- Define role-based access control using Elasticsearch Security
