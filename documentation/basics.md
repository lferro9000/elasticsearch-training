# Basic principles

When starting elasticsearch as we did on "[Setup a cluster](./setup.md)", we started a elasticsearch cluster with just one node.

# Elasticsearch cluster

An elasticsearch cluster is a set of nodes that coordinate between eachothers to hold your data and make it searcheable.

# Elasticsearch node

A node is one instance of elasticsearch. It will belong to a cluster.

A node may have multiple roles, depending on the configuration of itself and the cluster it belongs to.

# Node roles

# Multiple clusters

By default, elasticsearch clusters are isolated.

It is possible to perform both multi-cluster searchs and other tasks at a multi-cluster level.

However that kind of interconnectivity between clusters needs explicit configuration.

# Documents

Documents are the minimal data unit that is possible to store in Elasticsearch.

They are stored as json objects, with your data and metadata related to Elasticsearch internals.

The documents are stored in indices, normally of similar data.

# Index / Indices

A logical group of documents.

It can have what is called a mapping associated with it, which hints elasticsearch on how to handle the storing of the data for search purposes.

An index is divided internally in shards, which are setup either by default when the first document is created, or explicitly by creating the index before starting to import data into it.

The number of shards on an index is a complex setting to find, and depends on the data, size and structure of the documents and the nodes configuration due to data replication and

# Shards

The minimal active component in an elasticsearch node. Composed of an Lucene instance and related metadata.

When storing data, elasticsearch internally distributes the data between the existing shards.

# Snapshots
