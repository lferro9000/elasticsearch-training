# Standard Analyser

All text fields are processed by either the standard analyser of by a specific analyser as stated on the index mapper.

THIS is fundamental, and one reason why data may be "mangled" on ingestion without notice.

Ex.

If you pass the following value to a text field:

    30-01-68

This will be stored AS IS in the \_source of the record, but for search purposes, it will be parsed by the standard analyser, and be changed to 30 01 68 of NUM type.

This is why it is important to have correct and precise mappings that are well formed and tested with your data.

Please use the \_analize endpoint to test your content against the specified analysers as stated on the mappings for your index.

Reference ../requests/analyse_text_queries.http for more details on how to check the output of the analysers in play.

The data is not going to be missing, but you may not get it back as expected when searching.

> NOTE: Breaking changes on the analysers were introduced in 7.x version of Elasticsearch. Please reference https://www.elastic.co/guide/en/elasticsearch/reference/7.17/breaking-changes-7.0.html#breaking_70_analysis_changes for more details.
