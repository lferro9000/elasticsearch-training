# Summary

- [Requirements](./requirements.md)
- [Study materials for the Elasticsearch Engineer Certification](./introduction.md)
- [Setup a cluster](./setup.md)
- [Basic principles](./basics.md)
- [Authentication token](./authentication.md)
- [Inspecting the cluster](./inspect_cluster.md)
- [Add a new nodes ES 8.x](./add_nodes.md)
- [Add and delete indices](./add_delete_indices.md)
- [Document APIs](./document_apis.md)
- [Bulk requests for data ingestion](./bulk_api_call.md)
- [Standard Analyser](./analysers.md)
- [Random notes](./notes.md)
